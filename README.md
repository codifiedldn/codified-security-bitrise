For testing create .bitrise.secrets.yml with lines:

    envs:
    - CODIFIED_SECURITY_API_KEY: "<put test api key here>"

then call "bitrise run test"