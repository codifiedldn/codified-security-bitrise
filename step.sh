#!/bin/bash

set -e

export THIS_SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

RESTORE='\033[0m'
RED='\033[00;31m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
GREEN='\033[00;32m'

function color_echo {
	color=$1
	msg=$2
	echo -e "${color}${msg}${RESTORE}"
}

function echo_fail {
	msg=$1
	echo
	color_echo "${RED}" "${msg}"
	exit 1
}

function echo_warn {
	msg=$1
	color_echo "${YELLOW}" "${msg}"
}

function echo_info {
	msg=$1
	echo
	color_echo "${BLUE}" "${msg}"
}

function echo_details {
	msg=$1
	echo "  ${msg}"
}

function echo_done {
	msg=$1
	color_echo "${GREEN}" "  ${msg}"
}

function validate_required_input {
	key=$1
	value=$2
	if [ -z "${value}" ] ; then
		echo_fail "[!] Missing required input: ${key}"
	fi
}

#=======================================
# Main
#=======================================

echo_info "Configs:"
echo_details "* codified_security_api_key: ${codified_security_api_key}"
echo_details "* app_path: ${app_path}"
echo_details "* is_xamarin_ios: ${is_xamarin_ios}"

echo

validate_required_input "codified_security_api_key" ${codified_security_api_key}

binary_path="${app_path}"

if [ "${is_xamarin_ios}" = "true" ]; then
	validate_required_input "xamarin_ios_project_folder" ${xamarin_ios_project_folder}
	debug_path="${xamarin_ios_project_folder}/bin/iPhone/Debug";
	if [ -d $debug_path ]; then
  	zip -r app.zip $debug_path;
		binary_path="./app.zip"
	else
		echo_fail "Folder not found in $debug_path"
	fi
else
	validate_required_input "app_path" $binary_path
fi

echo_info "Sending request..."

curl -XPOST -F "file=@${binary_path}" "https://api.codifiedsecurity.com/scan?apiKey=${codified_security_api_key}"

echo_done "Done"